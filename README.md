# superfromhextojpgphpXtractor

Quick, dirty, and does what it was meant to do - extract any jpg files from any kind of archive. Just put the archive in raw/ folder, and run script with your browser. Remember to remove file extension, before you parse it. So instead archive.exe, rename it to just archive, m'kay?

It's mostly coded for fun so the code is pretty crazy here. Anyway it well served it's purpose while being fun to code.