
<style>
    :root{
        --value: 0%;
        --stringvalue: "0%";
    }
body, html {
    width: 100%;
    position: relative;
    padding: 0;
    margin: 0;
    padding-top: 100px;
}
p {
    padding: 0 20px;
}
.header::after {
    display: inline;
    content: var(--stringvalue);
}
.progressbar {
    width: var(--value);
    background-color: red;
    height: .4em;
}
.log {
    display: flex;
    flex-direction: column-reverse;
}
</style>
<body>
<p class="header" style="color:orange">program running </p>
<div class="progressbar"></div>
<div class="log">

<?php 

$files = scandir('raw/');
$fileNum = 1;

set_time_limit(3600);

foreach ($files as $file) {
    echo "<p>current path: $file";
    
    if(strpos($file, '.')===false){
        $data = fopen('raw/'.$file, 'rb');
        $size = filesize('raw/'.$file);
        echo "<p>current file size: ". filesize('raw/'.$file);
        if (!!$data) {
            $prev = null;
            $newFileContent = null;
            $fileNum = 1;
            $activeCopy = false;
            $i = 0;
            while (!feof($data)) {
                $char = fread ( $data , 1 );
                $i++;

                if($i % 120000 == 0){
                    echo "<style>:root{--value: ".round($i / $size * 100, 2)."%;--stringvalue: \"".round($i / $size * 100, 2)."%\"}</style>";
                    ob_flush();
                    flush();
                }

                if (!$activeCopy) {
                    if (bin2hex($prev) == 'ff' && bin2hex($char) == 'd8') {
                        $newFileContent = hex2bin('ff');
                        $newFileContent .= hex2bin('d8');
                        
                        $activeCopy = true;
                    }
                } else {
                    $newFileContent .= $char;
                    if (bin2hex($prev) == 'ff' && bin2hex($char) == 'd9') {
                        file_put_contents('export/file_'.str_pad($fileNum, 4, '0', STR_PAD_LEFT).'.jpg', $newFileContent);
                        echo "<p>copy finished, new file at: ".'export/file_'.str_pad($fileNum++, 4, '0', STR_PAD_LEFT).'.jpg';
                        $newFileContent = null;
                        $activeCopy = false;                        
                    }
                }
                $prev = $char;
            }
            echo "<style>:root{--value: 100%;--stringvalue: \"100%\"}</style>";
        }
    }

}

echo '</div>';